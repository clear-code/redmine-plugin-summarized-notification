# Copyright (C) 2021-2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module SummarizedNotification
  module UserPreferenceMixin
    class << self
      def prepended(base)
        base.safe_attributes("summarize_notification")
      end
    end

    def summarize_notification
      self[:summarize_notification] || "0"
    end

    def summarize_notification?
      summarize_notification == "1"
    end

    def summarize_notification=(value)
      self[:summarize_notification] = value
    end
  end

  class ViewListener < Redmine::Hook::ViewListener
    render_on :view_my_account_preferences,
              partial: "summarized_notification/users_preferences"
    render_on :view_users_form_preferences,
              partial: "summarized_notification/users_preferences"
  end
end

UserPreference.prepend(SummarizedNotification::UserPreferenceMixin)
